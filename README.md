# API #1 #

External API which is proxy to internal API #1

### How can I start service? ###

* set proper value in ```application.properties``` to connect internal API #1 - default localhost:8080
* ```mvn clean install```
* ```mvn spring-boot:run```

### Authentication ###
* there is Basic Auth - credentials: ```admin / password```

### Endpoints ###

* ```GET /api/days``` - returns customer who consumed the most energy and energy which the customer consumed per specific day
  eg. ```http://localhost:8081/api/days```

* ```GET /api/station-usages``` - returns data for each usage of charging station (possible filters: macAddress, userId,
  minEnergy, maxEnergy and paging by page (default 0) and size(default 10))
  eg. ```http://localhost:8081/api/station-usages?minEnergy=12&userId=2670&maxEnergy=12.3```
  and with paging ```http://localhost:8081/api/station-usages?minEnergy=12&userId=2670&maxEnergy=12.3&page=1&size=3```

* ```POST /api/station-usages``` - save new entry about charging station
  eg. ```curl -i -X POST -H "Content-Type: application/json" -d '{"stationName":"test", "macAddress":"1111", "startDate":"2011-11-09T22:46:00", "startTimeZone":"PL", "endDate":"2011-11-10T06:46:00", "endTimeZone":"PL", "transactionDate":"2011-11-10T06:46:00", "totalDuration":"05:00:00", "chargingTime":"08:00:00", "energy":10, "userId":"1234"}' http://localhost:8081/api/station-usages```

* ```GET /api/users``` - returns data about user usage like averageEnergyPerOneCharging, numberOfCharging, sumOfConsumedEnergy
  eg. ```http://localhost:8081/api/users```

### Audit ###
* Audit logs are in the console

### Caching and Throttling ###
* Caching and Throttling are set in ```application.yaml``` file
