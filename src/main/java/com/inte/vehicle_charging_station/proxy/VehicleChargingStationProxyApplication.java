package com.inte.vehicle_charging_station.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@SpringBootApplication
@EnableCaching
public class VehicleChargingStationProxyApplication {
    public static void main(String[] args) {
        SpringApplication.run(VehicleChargingStationProxyApplication.class, args);
    }
}
