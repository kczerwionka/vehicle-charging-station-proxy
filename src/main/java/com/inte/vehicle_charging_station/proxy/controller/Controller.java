package com.inte.vehicle_charging_station.proxy.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class Controller {
    private RestTemplate restTemplate;
    @Value("${api-service-url}")
    private String apiServiceUrl;

    public Controller() {
        this.restTemplate = new RestTemplate();
    }

    @GetMapping("/api/station-usages")
    public Object getStationUsages(HttpServletRequest request) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiServiceUrl + "/station-usages");
        request.getParameterMap().forEach(builder::queryParam);
        return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Object.class);
    }

    @PostMapping("/api/station-usages")
    public Object createStationUsages(HttpServletRequest request) throws IOException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String requestData = request.getReader().lines().collect(Collectors.joining());
        HttpEntity<String> httpRequest = new HttpEntity<>(requestData, httpHeaders);
        return restTemplate.exchange(apiServiceUrl + "/station-usages", HttpMethod.POST, httpRequest, Object.class);
    }

    @GetMapping("/api/days")
    public Object getDays(HttpServletRequest request) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiServiceUrl + "/days");
        request.getParameterMap().forEach(builder::queryParam);
        return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Object.class);
    }

    @GetMapping("/api/users")
    public Object getUsers(HttpServletRequest request) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiServiceUrl + "/users");
        request.getParameterMap().forEach(builder::queryParam);
        return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Object.class);
    }
}
